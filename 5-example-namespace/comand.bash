# Los namespaces permiten separar grupos de trabajo 

kubectl get namespaces

kubectl get ns

kubectl create ns curso

kubectl get ns curso -o yaml  # obtener en yaml el namespace curso

kubectl get ns

kubectl delete ns curso