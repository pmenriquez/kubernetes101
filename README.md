# kubeneter101

[Kubernetes](https://kubernetes.io/)

[Instalación kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

[Instalación de minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)

[Minikube online](https://kubernetes.io/docs/tutorials/hello-minikube/)

[Github minikube](https://github.com/kubernetes/minikube)

[kubectl autocompletado](https://kubernetes.io/docs/tasks/tools/install-kubectl/#optional-kubectl-configurations)

[kubectl CLI](https://kubernetes.io/docs/reference/kubectl/kubectl/)

[kubectl cheatsheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)
