# Kubernetes

# Minikube

# opción 1

minikube start --vm-driver=kvm2

# opción 1

minikube config set vm-driver kvm2

minikube start

# estado de clúster de minikube

minikube status

# para accer a minikube por ssh

minikube ssh

# kubectl

kubectl version

# Contexto de kubectl el de minikube

kubectl config use-context minikube

# comprobar que todo ha ido bien

kubectl cluster-info

# prueba

kubectl run hello-minikube --image=k8s.gcr.io/echoserver:1.10 --port=8080

# Creación de Pod

cd 1-examples/

kubectl create -f  pod.yaml

# ver Pods de namespace default

kubectl get pod

# ver todos Pods

kubectl get pod --all-namespaces

# ver Pods de un namespace específico

kubectl get pod -n example

# Creación de un deployment

kubectl create -f deployment.yaml

# Ver todos los deployments

kubectl get deployment --all-namespaces

# Escalado

kubectl scale deployment nginx-deployment --replicas=4

# Ver los deployments 

kubectl get deployment --all-namespaces -w 

# -w: matenerlo activo (watch)

# Parar deployment

kubectl delete deployment nginx-deployment

# cambiar versión de la imagen 

kubectl set image deployment nginx-deployment/nginx:1.16

# Example wordpress
# Creacion

kubectl create -f mysql-pvc.yaml

kubectl create -f mysql-deployment.yaml

kubectl create -f mysql-service.yaml

kubectl logs MYSQLPOD

kubectl create -f wordpress-pvc.yaml

kubectl create -f wordpress-deployment.yaml

kubectl create -f wordpress-service.yaml

# Borrado

kubectl delete -f mysql-pvc.yaml

kubectl delete -f mysql-deployment.yaml

kubectl delete -f mysql-service.yaml

kubectl delete -f wordpress-pvc.yaml

kubectl delete -f wordpress-deployment.yaml

kubectl delete -f wordpress-service.yaml

# dashboard

minikube dashboard

# ingress

minikube addons enable ingress

# Guestbook

cd 3-guestbook

kubectl create -f redis-master-deployment.yaml

kubectl create -f redis-slave-deployment.yaml

kubectl create -f redis-master-service.yaml

kubectl create -f redis-slave-service.yaml

kubectl create -f fornted-deployment.yaml

kubectl create -f fornted-service.yaml

# borrado
# Atención a la localización

kubectl delete -f 3-guestbook/


# example secrets 

kubectl create -f mysql-secret.yaml

kubectl create -f mysql-with-secret.yaml

kubectl create -f mysql-cm.yaml

kubectl create -f mysql-with-cm.yaml


